#include <iostream>
#include <iomanip>

using namespace std;

struct Person
{
    void printMe()
    {
        cout << m_name << ": " << m_age << " - " << m_dollarsInBank;
    }

    void addFunds(double deposit)
    {
        m_dollarsInBank = m_dollarsInBank + deposit;
    }

    std::string m_name;
    int m_age;
    double m_dollarsInBank;
    //double m_assets;
    //Car m_car;   /// Maybe?????
};

int main()
{
    // Perhaps look up: constructors ?

    Person drew;
    Person alex;

    drew.m_name = "Drew";
    drew.m_age  = 18;
    drew.m_dollarsInBank = 10;

    alex.m_name = "Alex";
    alex.m_age = 31;
    alex.m_dollarsInBank = 0;


    alex.addFunds(10.57);
    drew.addFunds(33.88);


    drew.printMe();
    cout << endl;
    alex.printMe();

    return 0;
}